require 'rails_helper'

RSpec.describe Skill, type: :model do
  context "try to search" do
    let!(:pepper_1) { Skill.create!(name: "Pepper") }
    let!(:salami) { Skill.create!(name: "Salami") }
    let!(:cheese) { Skill.create!(name: "Cheese") }
    let!(:pepper_2) { Skill.create!(name: "Salami & Pepper") }
    let!(:pepper_3) { Skill.create!(name: "Cheese & Pepper") }

    it "can find all peppers" do
      results = Skill.search_by_name("Pepper").to_a
      expect(results).to contain_exactly(pepper_1, pepper_2, pepper_3)
    end
  end
end
