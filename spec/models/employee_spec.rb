require 'rails_helper'

RSpec.describe Employee, type: :model do
  let(:contact_email) { "some@email.com" }
  let(:contact_phone) { "+79999999999" }
  let(:name) { "петр петрович петров" }
  let(:salary) { 1000000 }
  context "name validations" do
    it "pass validation with valid name" do
      expect(Employee.new(name: name, contact: contact_email, salary: salary)).to be_valid
    end
    it "fails validation with name including non-cyrillic symbols" do
      expect(Employee.new(name: "петр петрович petrov", contact: contact_email, salary: salary)).to be_invalid
    end
    it "fails validation with name including not 3 words" do
      expect(Employee.new(name: "петр петрович", contact: contact_email, salary: salary)).to be_invalid
    end
  end

  context "contact validations" do
    it "pass validation with email contact info" do
      expect(Employee.new(name: name, contact: contact_email, salary: salary)).to be_valid
    end
    it "pass validation with phone contact info" do
      expect(Employee.new(name: name, contact: contact_phone, salary: salary)).to be_valid
    end
    it "fails validation with wrong contact" do
      expect(Employee.new(name: name, contact: "+7312321", salary: salary)).to be_invalid
      expect(Employee.new(name: name, contact: "mailsone@", salary: salary)).to be_invalid
    end
  end

  it "pass validation with unspecified salary" do
    expect(Employee.new(name: name, contact: contact_email, salary: nil)).to be_valid
  end

  context "try to find suitable vacancies" do
    let!(:red) { create(:red) }
    let!(:blue) { create(:blue) }
    let!(:green) { create(:green) }
    let!(:yellow) { create(:yellow) }
    let!(:black) { create(:black) }
    let!(:white) { create(:white) }
    let(:first_employee) { create(:first_employee) { |em| em.skills.push(red, green, blue) } }
    let(:second_employee) { create(:second_employee) { |em| em.skills.push(yellow, black, white) } }
    let!(:first_vacancy) { create(:first_vacancy) { |vac| vac.skills.push(red, green, blue, white) } }
    let!(:second_vacancy) { create(:second_vacancy) { |vac| vac.skills.push(yellow, black) } }

    it "returns one vacancy for first employee" do
      expect(first_employee.suitable_vacancies.to_a).to contain_exactly(first_vacancy)
    end
    it "returns two vacancies for second employee" do
      expect(second_employee.suitable_vacancies.to_a).to contain_exactly(first_vacancy, second_vacancy)
    end
  end
end
