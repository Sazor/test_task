require 'rails_helper'

RSpec.describe Vacancy, type: :model do
  context "close date validations" do
    it "fails if close date in the past" do
      expect(Vacancy.new(title: "title", contact: "contact", close_on: 1.day.ago)).to be_invalid
    end
  end

  context "try to find suitable employees" do
    let!(:red) { create(:red) }
    let!(:blue) { create(:blue) }
    let!(:green) { create(:green) }
    let!(:yellow) { create(:yellow) }
    let!(:black) { create(:black) }
    let!(:white) { create(:white) }
    let!(:first_employee) { create(:first_employee) { |em| em.skills.push(red, green, blue) } }
    let!(:second_employee) { create(:second_employee) { |em| em.skills.push(yellow, black, white) } }
    let(:first_vacancy) { create(:first_vacancy) { |vac| vac.skills.push(red, green, blue, white) } }
    let(:second_vacancy) { create(:second_vacancy) { |vac| vac.skills.push(yellow, black) } }

    it "returns one employee for first vacancy" do
      expect(first_vacancy.suitable_employees.to_a).to contain_exactly(first_employee, second_employee)
    end
    it "returns two employees for second vacancy" do
      expect(second_vacancy.suitable_employees.to_a).to contain_exactly(second_employee)
    end
  end
end
