FactoryGirl.define do
  factory :red, class: Skill do
    name "red"
  end
  factory :green, class: Skill do
    name "green"
  end
  factory :blue, class: Skill do
    name "blue"
  end
  factory :yellow, class: Skill do
    name "yellow"
  end
  factory :black, class: Skill do
    name "black"
  end
  factory :white, class: Skill do
    name "white"
  end
end