FactoryGirl.define do
  factory :first_employee, class: Employee do
    name      "Алексей Алексеевич Алексеев"
    contact   "email@email.com"
  end
  factory :second_employee, class: Employee do
    name      "Петр Петрович Петров"
    contact   "email@email.com"
  end
end