FactoryGirl.define do
  factory :first_vacancy, class: Vacancy do
    title     "First vacancy"
    contact   "Some email"
    close_on  { Date.today + 1.week }
  end
  factory :second_vacancy, class: Vacancy do
    title     "Second vacancy"
    contact   "Some email"
    close_on  { Date.today + 1.week }
  end
end