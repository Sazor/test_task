Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    resources :skills, only: [:index, :create, :destroy] do
      get 'search', on: :collection
    end
    resources :vacancies, only: [:index, :create, :destroy, :update, :show]
    resources :employees, only: [:index, :create, :destroy, :update, :show]
  end
end
