var app = angular.module('bluefox', ['ui.router', 'rails', 'angularModalService', 'xeditable', 'ui.bootstrap', 'ngTagsInput', 'ui-notification'])
.config([
'$stateProvider',
'$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('employees', {
      url: '/employees',
      templateUrl: 'employees/employees.html',
      controller: 'EmployeesCtrl'
    })
    .state('vacancies', {
      url: '/vacancies',
      templateUrl: 'vacancies/vacancies.html',
      controller: 'VacanciesCtrl'
    });

  $urlRouterProvider.otherwise('employees');
}])
.config(function(NotificationProvider) {
  NotificationProvider.setOptions({
      delay: 10000,
      startTop: 20,
      startRight: 10,
      verticalSpacing: 20,
      horizontalSpacing: 20,
      positionX: 'right',
      positionY: 'bottom'
  });
});