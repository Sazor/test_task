app.factory('Vacancy', [
  'railsResourceFactory',
  '$http',
  function(railsResourceFactory, $http){
    return railsResourceFactory({
        url: 'api/vacancies',
        name: 'vacancy',
        pluralName: 'vacancies'
    });
  }
])