app.controller('VacanciesCtrl', [
  '$scope',
  '$http',
  'Vacancy',
  'ModalService',
  '$q',
  'Notification',
  function($scope, $http, Vacancy, ModalService, $q, Notification){
    $scope.vacancies = [];
    $scope.disableEdit = [];
    Vacancy.query().then(function(results) {
      $scope.vacancies = results;
    })
    .catch(function(data) {
      Notification.error({message: data.data.errors.join(), delay: 10000});
    });
    $scope.cancelEdit = function(rowform, index) {
      if(typeof $scope.vacancies[index].id == 'undefined') {
        $scope.vacancies.splice(index, 1);
      }
      rowform.$cancel();
    }
    $scope.editSkills = function(index) {
      $scope.vacancies[index].createdAt = new Date($scope.vacancies[index].createdAt);
      $scope.vacancies[index].closeOn = new Date($scope.vacancies[index].closeOn);
      $scope.disableEdit[index] = false;
    };
    $scope.disableEditSkills = function(index) {
      $scope.disableEdit[index] = true;
    };
    $scope.isEditSkillDisabled = function(index) {
      if(typeof $scope.disableEdit[index] == 'undefined') {
        $scope.disableEdit[index] = true;
      }
      return $scope.disableEdit[index];
    };
    $scope.checkTitle = function(title) {
      if(title === "") {
        return "You should fill title field";
      }
    };
    $scope.checkContact = function(contact) {
      if(contact === "") {
        return "You should fill contact field";
      }
    };
    $scope.saveVacancy = function(data, id, index) {
      angular.extend(data, { id: id });
      skills = [];
      if(typeof $scope.vacancies[index].skills != 'undefined') {
        $scope.vacancies[index].skills.forEach(function(item, i, arr) {
          if("id" in item) {
            skills.push({ skill_id: item.id });
          }
          else {
            skills.push({ skill_attributes: item });
          }
        })
      };
      angular.extend(data, { skills_vacancies_attributes: skills });
      vacancy = new Vacancy(data);
      vacancy.save().then(function(result) {
        $scope.vacancies[index].id = result.id;
      })
      .catch(function(data) {
        if(typeof vacancy.id == 'undefined') {
          $scope.vacancies.splice(index, 1);
        }
        Notification.error({message: data.data.errors.join(), delay: 10000});
      });
    };
    $scope.removeVacancy = function(index, id) {
      vacancy = new Vacancy({ id: id });
      vacancy.remove().then(function(result) {
        $scope.vacancies.splice(index, 1);
      });
    };
    $scope.addVacancy = function() {
      $scope.inserted = {
        title: '',
        contact: ''
      };
      $scope.vacancies.push($scope.inserted);
    };
    $scope.loadSkills = function(query) {
      var deferred = $q.defer();
      $http.get('/api/skills/search?text=' + query).success(function(data){
        deferred.resolve(data.skills);
      });
      return deferred.promise;
    };
    $scope.getSuitableEmployees = function(vacancy) {
      Vacancy.get(vacancy.id).then(function (vac) {
        ModalService.showModal({
          templateUrl: "vacancies/_suitable-employees.html",
          controller: "SuitableEmployeesCtrl",
          inputs: {
            suitableEmployees: vac.vacancyWithEmployees.employees
          }
        }).then(function(modal) {
          modal.element.modal();
        })
        .catch(function(data) {
          Notification.error({message: data.data.errors.join(), delay: 10000});
        });
      });
    };
  }
])