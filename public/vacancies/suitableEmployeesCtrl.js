app.controller('SuitableEmployeesCtrl',
  function($scope, suitableEmployees, close) {
    $scope.suitableEmployees = suitableEmployees;
    $scope.dismissModal = function(result) {
      close(result, 200);
    }
  }
);