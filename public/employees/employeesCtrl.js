app.controller('EmployeesCtrl', [
  '$scope',
  '$http',
  'Employee',
  'ModalService',
  '$q',
  'Notification',
  function($scope, $http, Employee, ModalService, $q, Notification){
    $scope.employees = [];
    $scope.disableEdit = [];
    Employee.query().then(function(results) {
      $scope.employees = results;
    });
    $scope.cancelEdit = function(rowform, index) {
      if(typeof $scope.employees[index].id == 'undefined') {
        $scope.employees.splice(index, 1);
      }
      rowform.$cancel();
    }
    $scope.editSkills = function(index) {
      $scope.disableEdit[index] = false;
    };
    $scope.disableEditSkills = function(index) {
      $scope.disableEdit[index] = true;
    };
    $scope.isEditSkillDisabled = function(index) {
      if(typeof $scope.disableEdit[index] == 'undefined') {
        $scope.disableEdit[index] = true;
      }
      return $scope.disableEdit[index];
    };
    $scope.checkName = function(name) {
      var reg = /^[\u0430-\u044F\u0410-\u042F]+ [\u0430-\u044F\u0410-\u042F]+ [\u0430-\u044F\u0410-\u042F]+$/;
      if(!reg.test(name)) {
        return "Only 3 words with cyrillic symbols";
      }
    };
    $scope.checkContact = function(contact) {
      var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var phone = /^\+[0-9]{11,15}$/;
      if(!email.test(contact) && !phone.test(contact)) {
        return "Contact should be phone number or email"
      }
    };
    $scope.saveEmployee = function(data, id, index) {
      angular.extend(data, { id: id });
      skills = [];
      if(typeof $scope.employees[index].skills != 'undefined') {
        $scope.employees[index].skills.forEach(function(item, i, arr) {
          if("id" in item) {
            skills.push({ skill_id: item.id });
          }
          else {
            skills.push({ skill_attributes: item });
          }
        })
      };
      angular.extend(data, { employees_skills_attributes: skills });
      employee = new Employee(data);
      employee.save().then(function(result) {
        $scope.employees[index].id = result.id;
      })
      .catch(function(data) {
        Notification.error({message: data.data.errors.join(), delay: 10000});
      });
    };
    $scope.removeEmployee = function(index, id) {
      employee = new Employee({ id: id });
      employee.remove().then(function(result) {
        $scope.employees.splice(index, 1);
      })
      .catch(function(data) {
        if(typeof employee.id == 'undefined') {
          $scope.employees.splice(index, 1);
        }
        Notification.error({message: data.data.errors.join(), delay: 10000});
      });
    };
    $scope.addEmployee = function() {
      $scope.inserted = {
        name: '',
        contact: '',
        active: true
      };
      $scope.employees.push($scope.inserted);
    };
    $scope.loadSkills = function(query) {
      var deferred = $q.defer();
      $http.get('/api/skills/search?text=' + query).success(function(data){
        deferred.resolve(data.skills);
      });
      return deferred.promise;
    };
    $scope.getSuitableVacancies = function(employee) {
      Employee.get(employee.id).then(function (emp) {
        ModalService.showModal({
          templateUrl: "employees/_suitable-vacancies.html",
          controller: "SuitableVacanciesCtrl",
          inputs: {
            suitableVacancies: emp.employeeWithVacancies.vacancies
          }
        }).then(function(modal) {
          modal.element.modal();
        })
        .catch(function(data) {
          Notification.error({message: data.data.errors.join(), delay: 10000});
        });
      });
    };
  }
])