app.factory('Employee', [
  'railsResourceFactory',
  '$http',
  function(railsResourceFactory, $http){
    return railsResourceFactory({
        url: 'api/employees',
        name: 'employee'
    });
  }
])