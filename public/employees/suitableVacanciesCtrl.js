app.controller('SuitableVacanciesCtrl',
  function($scope, suitableVacancies, close) {
    $scope.suitableVacancies = suitableVacancies
    $scope.dismissModal = function(result) {
      close(result, 200);
    }
  }
);