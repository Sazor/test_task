class Vacancy < ActiveRecord::Base
  validates :title, presence: true
  validates :contact, presence: true
  validates :salary, numericality: { greater_than: 0 }, allow_nil: true
  validate :close_date_cannot_be_in_the_past
  has_many :skills_vacancies, dependent: :delete_all
  has_many :skills, through: :skills_vacancies
  accepts_nested_attributes_for :skills_vacancies

  scope :by_salary_desc, -> { order(salary: :desc) }

  def suitable_employees
    Employee.joins(skills: :vacancies).where("vacancies.id = ?", id).distinct.by_salary_asc
  end

  private
  def close_date_cannot_be_in_the_past
    if close_on.present? && close_on < (created_at || Date.today)
      errors.add(:close_on, "can't be in the past")
    end
  end
end
