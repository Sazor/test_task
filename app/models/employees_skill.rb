class EmployeesSkill < ActiveRecord::Base
  belongs_to :employee
  belongs_to :skill
  accepts_nested_attributes_for :skill

end
