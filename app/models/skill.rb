class Skill < ActiveRecord::Base
  include PgSearch
  pg_search_scope :search_by_name, against: :name, using: { tsearch: { prefix: true } }

  validates :name, presence: true, uniqueness: true
  has_many :employees_skills, dependent: :delete_all
  has_many :skills_vacancies, dependent: :delete_all
  has_many :employees, through: :employees_skills
  has_many :vacancies, through: :skills_vacancies
end
