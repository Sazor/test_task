class SkillsVacancy < ActiveRecord::Base
  belongs_to :vacancy
  belongs_to :skill

  accepts_nested_attributes_for :skill
end
