class Employee < ActiveRecord::Base
  validates :name, presence: true, format: { with: /\A[а-яА-Я]+ [а-яА-Я]+ [а-яА-Я]+\z/, message: "Only 3 words with cyrillic symbols" }
  validates :salary, numericality: { greater_than: 0 }, allow_nil: true
  validates :contact, contact: true
  has_many :employees_skills, dependent: :delete_all
  has_many :skills, through: :employees_skills
  accepts_nested_attributes_for :employees_skills

  scope :by_salary_asc, -> { order(salary: :asc) }

  def suitable_vacancies
    Vacancy.joins(skills: :employees).where("employees.id = ?", id).distinct.by_salary_desc
  end
end
