class ContactValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless EmailValidator.valid?(value) || value =~ /\A\+[0-9]{11,15}\z/
      record.errors[attribute] << (options[:message] || "is not an email or phone number")
    end
  end
end