class VacancyWithEmployeesSerializer < ActiveModel::Serializer
  attributes :id, :title, :close_on, :salary, :contact, :created_at
  has_many :skills
  has_many :employees

  def employees
    object.suitable_employees
  end
  def created_at
    object.created_at.to_date
  end
end
