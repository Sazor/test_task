class EmployeeWithVacanciesSerializer < ActiveModel::Serializer
  attributes :id, :name, :contact, :active, :salary
  has_many :skills
  has_many :vacancies

  def vacancies
    object.suitable_vacancies
  end
end
