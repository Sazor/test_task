class EmployeeSerializer < ActiveModel::Serializer
  attributes :id, :name, :contact, :active, :salary
  has_many :skills
end
