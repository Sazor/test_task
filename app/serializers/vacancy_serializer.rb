class VacancySerializer < ActiveModel::Serializer
  attributes :id, :title, :close_on, :salary, :contact, :created_at
  has_many :skills
  def created_at
    object.created_at.to_date
  end
end
