class Api::EmployeesController < ApplicationController
  def index
    employees = Employee.page(params[:page])
    render json: employees, serializer: PaginationSerializer
  end

  def show
    employee = Employee.find(params[:id])
    render json: employee, serializer: EmployeeWithVacanciesSerializer
  end

  def create
    employee = Employee.new(employee_params)
    if employee.save
      render json: employee, serializer: EmployeeWithVacanciesSerializer
    else
      render json: { errors: employee.errors.full_messages }, status: 422
    end
  end

  def update
    employee = Employee.find(params[:id])
    employee.employees_skills.delete_all
    if employee.update_attributes(employee_params)
      render json: employee, serializer: EmployeeWithVacanciesSerializer
    else
      render json: { errors: employee.errors.full_messages }, status: 422
    end
  end

  def destroy
    employee = Employee.find(params[:id])
    employee.destroy
    render json: { message: "Employee was successfully destroyed"}
  end

  private
  def employee_params
    params.require(:employee).permit(:name, :contact, :salary, :active, employees_skills_attributes: [:skill_id, skill_attributes: [:name]])
  end
end
