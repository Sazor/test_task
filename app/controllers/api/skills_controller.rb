class Api::SkillsController < ApplicationController
  def index
    skills = Skill.page(params[:page])
    render json: skills, serializer: PaginationSerializer
  end

  def create
    skill = Skill.new(skill_params)
    if skill.save
      render json: skill
    else
      render json: { errors: skill.errors.full_messages }, status: 422
    end
  end

  def destroy
    skill = Skill.find(params[:id])
    skill.destroy
    render json: { message: "Skill was successfully destroyed"}
  end

  def search
    skills = Skill.search_by_name(params[:text])
    render json: skills
  end

  private
  def skill_params
    params.require(:skill).permit(:name)
  end
end
