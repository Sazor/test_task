class Api::VacanciesController < ApplicationController
  def index
    vacancies = Vacancy.page(params[:page])
    render json: vacancies, serializer: PaginationSerializer
  end

  def show
    vacancy = Vacancy.find(params[:id])
    render json: vacancy, serializer: VacancyWithEmployeesSerializer
  end

  def create
    vacancy = Vacancy.new(vacancy_params)
    if vacancy.save
      render json: vacancy, serializer: VacancyWithEmployeesSerializer
    else
      render json: { errors: vacancy.errors.full_messages }, status: 422
    end
  end

  def update
    vacancy = Vacancy.find(params[:id])
    vacancy.skills_vacancies.delete_all
    if vacancy.update_attributes(vacancy_params)
      render json: vacancy, serializer: VacancyWithEmployeesSerializer
    else
      render json: { errors: vacancy.errors.full_messages }, status: 422
    end
  end

  def destroy
    vacancy = Vacancy.find(params[:id])
    vacancy.destroy
    render json: { message: "Vacancy was successfully destroyed"}
  end

  private
  def vacancy_params
    params.require(:vacancy).permit(:title, :contact, :salary, :created_at, :close_on, skills_vacancies_attributes: [:skill_id, skill_attributes: [:name]])
  end
end
