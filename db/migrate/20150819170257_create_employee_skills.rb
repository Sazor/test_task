class CreateEmployeeSkills < ActiveRecord::Migration
  def change
    create_join_table :employees, :skills
  end
end
