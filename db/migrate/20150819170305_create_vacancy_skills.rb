class CreateVacancySkills < ActiveRecord::Migration
  def change
    create_join_table :vacancies, :skills
  end
end
