class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :title, required: true
      t.date :close_on
      t.integer :salary
      t.text :contact, required: true

      t.timestamps null: false
    end
  end
end
