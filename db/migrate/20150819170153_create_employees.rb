class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name, required: true
      t.string :contact, required: true
      t.boolean :active, default: true
      t.integer :salary

      t.timestamps null: false
    end
  end
end
