# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150819170305) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "employee_skills", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "skill_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "employee_skills", ["employee_id"], name: "index_employee_skills_on_employee_id", using: :btree
  add_index "employee_skills", ["skill_id"], name: "index_employee_skills_on_skill_id", using: :btree

  create_table "employees", force: :cascade do |t|
    t.string   "name"
    t.string   "contact"
    t.boolean  "active",     default: true
    t.integer  "salary"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "employees_skills", id: false, force: :cascade do |t|
    t.integer "employee_id", null: false
    t.integer "skill_id",    null: false
  end

  create_table "skills", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "skills", ["name"], name: "index_skills_on_name", using: :btree

  create_table "skills_vacancies", id: false, force: :cascade do |t|
    t.integer "vacancy_id", null: false
    t.integer "skill_id",   null: false
  end

  create_table "vacancies", force: :cascade do |t|
    t.string   "title"
    t.date     "close_on"
    t.integer  "salary"
    t.text     "contact"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
